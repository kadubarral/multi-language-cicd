CONTEXT=$(kubectl config current-context)

TOKEN=$(kubectl get secret --context $CONTEXT \
   $(kubectl get serviceaccount cicd-service-account \
       --context $CONTEXT \
       -n cicd \
       -o jsonpath='{.secrets[0].name}') \
   -n cicd \
   -o jsonpath='{.data.token}' | base64 --decode)

kubectl config set-credentials ${CONTEXT}-token-user --token $TOKEN

kubectl config set-context $CONTEXT --user ${CONTEXT}-token-user
