package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/hellogo", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello Go!")
	})

	http.HandleFunc("/hellogo/healthz", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Healthy!")
	})

	http.ListenAndServe(":8080", nil)
}
